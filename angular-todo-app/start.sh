#!/bin/bash
docker run -p 4200:4200 --expose 4200 -it --rm --name angular-container -v "$PWD":/usr/src/app -w /usr/src/app node:8 /bin/bash -c "npm install; npm start"
