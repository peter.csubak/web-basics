const path = require('path');

module.exports = {
    mode: 'development',
    entry: './app/index.js',
    output: {
        path: __dirname,
        filename: 'index_bundle.js'
    },
    module: {
        rules: [
            { test: /\.js$/, loader: 'babel-loader', exclude: /node_modules/ }
        ]
    },
    devServer: {
        historyApiFallback: true,
        contentBase: './',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        port: '4300'
    }
};
