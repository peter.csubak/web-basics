#!/bin/bash
docker run -p 4300:4300 --expose 4300 -it --rm --name react-container -v "$PWD":/usr/src/app -w /usr/src/app node:8 /bin/bash -c "npm install; npm start"
