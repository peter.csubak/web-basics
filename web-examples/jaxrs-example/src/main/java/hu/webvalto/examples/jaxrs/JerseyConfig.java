package hu.webvalto.examples.jaxrs;

import io.swagger.jaxrs.config.BeanConfig;
import io.swagger.jaxrs.listing.ApiListingResource;
import io.swagger.jaxrs.listing.SwaggerSerializers;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.wadl.internal.WadlResource;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.ws.rs.ApplicationPath;

@Component
@ApplicationPath("/api")
public class JerseyConfig extends ResourceConfig {

    public JerseyConfig() {
        register(CORSResponseFilter.class);
        register(TodoEndpoint.class);
    }

    @PostConstruct
    public void init() {
        swaggerConfig();
    }

    private void swaggerConfig() {
        register(ApiListingResource.class);
        register(SwaggerSerializers.class);

        BeanConfig swaggerConfigBean = new BeanConfig();
        swaggerConfigBean.setConfigId("Todo app backend");
        swaggerConfigBean.setTitle("Todo app backend");
        swaggerConfigBean.setVersion("v1");
        swaggerConfigBean.setResourcePackage(TodoEndpoint.class.getPackage().getName());
        swaggerConfigBean.setHost("localhost:8080");
        swaggerConfigBean.setBasePath("/api");
        swaggerConfigBean.setSchemes(new String[] {"http"});
        swaggerConfigBean.setPrettyPrint(true);
        swaggerConfigBean.setScan(true);
    }

}
