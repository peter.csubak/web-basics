package hu.webvalto.example.tcpchat;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class ChatServer {

    private Map<Socket, PrintWriter> clients;
    private boolean exitStatus;
    private ServerSocket serverSocket;

    public static void main(String[] args) {
        new ChatServer().startServer();
    }

    private ChatServer() {
        clients = new HashMap<>();
    }

    private void startServer() {
        startMainThread();
        exitStatus = false;
        Scanner input = new Scanner(System.in);
        while (input.hasNextLine()) {
            String line = input.nextLine();

            if (line.equalsIgnoreCase("exit")) {
                writeLine("Bye all!");
                doExit();
                break;
            }

            writeLine("ChatServer: " + line);
        }
    }

    private void writeLine(String line) {
        clients.entrySet().stream()
            .filter(entry -> !entry.getKey().isClosed())
            .forEach(entry -> {
                entry.getValue().write(line + "\n");
                entry.getValue().flush();
            });
    }

    private synchronized void doExit() {
        System.out.println("Exiting...");
        exitStatus = true;
        clients.forEach((client, writer) -> {
            if (!client.isClosed()) {
                try {
                    writer.close();
                    client.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        try {
            serverSocket.close();
        } catch (IOException ignore) {
        }
    }

    private void startMainThread() {
        new Thread(() -> {
            try (ServerSocket server = new ServerSocket(21111)) {
                serverSocket = server;
                System.out.println("Szerver cím: " + server.getInetAddress().getHostAddress() + " port: " + server.getLocalPort());
                while (!exitStatus) {
                    final Socket socket = server.accept();
                    startClientThread(socket);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }).start();
    }

    private void startClientThread(Socket socket) {
        new Thread(() -> {
            try {
                System.out.println("Új kapcsolat: " + socket.getInetAddress().getHostAddress());
                socket.setTcpNoDelay(true);
                clients.put(socket, new PrintWriter(socket.getOutputStream()));
                Scanner scanner = new Scanner(socket.getInputStream());
                while (scanner.hasNextLine()) {
                    String line = scanner.nextLine();
                    System.out.println(line);
                    writeLine(line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }

}
