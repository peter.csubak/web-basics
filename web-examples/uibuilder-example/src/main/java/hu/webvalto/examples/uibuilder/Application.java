package hu.webvalto.examples.uibuilder;

import io.devbench.uibuilder.annotations.EnableUIBuilder;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"hu.webvalto.examples.uibuilder"})
@EnableUIBuilder("hu.webvalto.examples.uibuilder")
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
